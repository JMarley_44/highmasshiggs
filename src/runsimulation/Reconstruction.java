package runsimulation;

public class Reconstruction
{
    // constructor 
    //Reconstruction(Particle p) { }
    
    // method to retrieve position  - now covered in RunSimulation

      static double getPositionx1(Particle p, double param1) // par = 1.305
    {
        return p.x;
    } 
       static double getPositiony1(Particle p, double param1) // par = 1.305
    {
        return p.y;
    } 
        static double getPositionx2(Particle p, double param2) // par = 1.405
    {
        return p.x;
    } 
         static double getPositiony2(Particle p, double param2) // par = 1.405
    {
        return p.y;
    } 
          static double getPositionx3(Particle p, double param3) // par = 1.505
    {
        return p.x;
    } 
           static double getPositiony3(Particle p, double param3) // par = 1.505
    {
        return p.y;
    } 
       
    static double getm12 (double y_det2, double y_det1, double x_det2, double x_det1)   //this method is called in RunSimulation
    {
        double m12 = (y_det2-y_det1)/(x_det2-x_det1);
        return m12;
    }
    
    static double getm13 (double y_det3, double y_det1, double x_det3, double x_det1)   //this method is called in RunSimulation
    {
        double m13 = (y_det3-y_det1)/(x_det3-x_det1);
        return m13;
    }
    
    static double getm23 (double y_det3, double y_det2, double x_det3, double x_det2)   //this method is called in RunSimulation
    {
        double m23 = (y_det3-y_det2)/(x_det3-x_det2);
        return m23;
    }
    
    static double getm0o (double y_det0, double x_det0)   //this method is called in RunSimulation
    {
        double m0o = (0-y_det0)/(0-x_det0);
        return m0o;
    }

    static double getC (double y_det, double x_det, double m)   //this method is called in RunSimulation
    {
        double C = y_det-(m*x_det);
        return C;
    }
    
    static double average3In (double in1, double in2, double in3)   //Averages 3 inputs, to be used for m and c
    {
        double avg = (in1+in2+in3)/3;
        return avg;
    }
    
    //The following methods solve the quadratic y=mx+c subs. into x^2+y^2=R^2 for R=1.2
    static double quadraticXPositive (double m, double c)
    {
        double quad = (-2*c*m)+Math.sqrt((4*c*c*m*m)-(4*(1+(m*m))*((c*c)-(1.2*1.2))));
        double quadratic = quad/(2*(1+(m*m)));
        return quadratic;
    }
    
    static double quadraticXNegative (double m, double c)
    {
        double quad = (-2*c*m)-Math.sqrt((4*c*c*m*m)-(4*(1+(m*m))*((c*c)-(1.2*1.2))));
        double quadratic = quad/(2*(1+(m*m)));
        return quadratic;
    }

    static double quadraticYPositive (double m, double c)
    {
        double quad = (-2*c*m)+Math.sqrt((4*c*c*m*m)-(4*(1+(m*m))*((c*c)-((1.2*1.2)*(m*m)))));
        double quadratic = quad/(2*(1+(m*m)));
        return quadratic;
    }
    
    static double quadraticYNegative (double m, double c)
    {
        double quad = (-2*c*m)-Math.sqrt((4*c*c*m*m)-(4*(1+(m*m))*((c*c)-((1.2*1.2)*(m*m)))));
        double quadratic = quad/(2*(1+(m*m)));
        return quadratic;
    }
    
    //This method finds the closest from the quadratic to the detected value 
    public static double [] getClosest(double detx, double dety, double posx, double negx, double posy, double negy)
    {  
        //Setup arrays for storage of the output variables (positive or negative co-ords)
        double [] answer_xy = new double [2];
        //Square the difference to see which is further from detected value
            double neg1 = (negx-detx)*(negx-detx);
            double neg2 = (negy-dety)*(negy-dety);
            double pos1 = (posx-detx)*(posx-detx);
            double pos2 = (posy-dety)*(posy-dety);
            //If closer to positive distance than negative return postivie co-ordinates for x
            if (pos1<neg1)
            {
                answer_xy[0] = posx;
            }
            else
            //Else return negative co-ordinates for x
            {
                answer_xy[0] = negx;
            }
            //If closer to positive distance than negative return postivie co-ordinates for y
            if (pos2<neg2)
            {
                answer_xy[1] = posy;
            }
            else
            //Else return negative co-ordinates for y
            {
                answer_xy[1] = negy;
            }
            return answer_xy;   //Returns the array in form x as [0], y as [1]
    }  
    
    static double getdeltarecon(double x_det0, double y_det0, double m)
    {
        double Phi = getPhirecon(x_det0, y_det0);
        double mangle = Math.atan(m);
        double delta = Phi - mangle;
        if (delta > Math.PI/2.)
            delta = delta - Math.PI;
        if (delta < -Math.PI/2.)
            delta = delta + Math.PI;
        return delta;
    }
    
    static double getTheta(double x, double y , double z){
        double r = Math.sqrt(x*x+y*y);
        double Theta = Math.atan2(r, z);
        return Theta;
    }
    
    static double getQrecon(double deltarecon) 
    {
    double Q = deltarecon/Math.abs(deltarecon);
    return Q;
    }
    
    static double getPhirecon(double x_det0, double y_det0)
    {
    //double Phi = Math.acos(((1.2*1.2)+(1.2*1.2)-(Math.sqrt((y_det0*y_det0)+((x_det0-1.2)*(x_det0-1.2)))))/(2*1.2*1.2));
    double Phi = Math.atan2(y_det0, x_det0);
    return Phi;
    }
    
    static double getphirecon(double Phirecon , double deltarecon)
    {
    double phi = Phirecon - deltarecon;
    return phi;
    }
    
    static double getPperprecon(double Qrecon, double deltarecon)
    {
    double Pperp = (300*4*1.2*Qrecon)/(2*deltarecon);
    return Pperp;
    }
    
    static double getErecon(double pz0, double Pperprecon)
    {
    double m = 106.;
    double E = Math.sqrt(m*m+pz0*pz0+Pperprecon*Pperprecon);
    //System.out.println("pz0 is: " + pz0/1E3 + " GeV");
    return E;
    }
}



