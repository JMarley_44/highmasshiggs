package runsimulation;
class MCS
{
    double rho;
    double Z;
    double A;
    double thickness;
    double me;
    
            
    public MCS(double rho_in, double Z_in, double A_in)
    {
        rho = rho_in;
        Z = Z_in;
        A = A_in;
    }

    public double getX0()
    {
                double X0 = (716.4*A)/(rho*Z*(Z+1)*100*(Math.log(287/Math.sqrt(Z)))); //in m
        return X0;
    }

    public double getTheta0(Particle part, double x)
    {
        if( A==0 || Z == 0){ 
            return 0;}
        thickness = x;
        // shall return Theta0 for material thickness x
        double Theta0 = (13.6*part.Q*(Math.sqrt(x/getX0()))*(1+(0.038*Math.log(x/getX0()))))/(part.beta()*part.momentum());
        return Theta0;
    }
    
   
    }
