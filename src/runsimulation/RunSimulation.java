package runsimulation;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

class RunSimulation
{
       static Random randGen = new Random();
       static final int Npart = 4;
    // Program to run a simulation of particles in a "experiment"

    // The program makes use of the class Particle (almost identical to the one used in week 4)
    // to store the components of the position fourvector (time, x, y, z)
    // and the four-momentum (technically stored as mass and px, py, pz)

    // The particle tracking is performed by the class ParticleTracker,
    // which is almost identical to the class used in week 4, however it is extended
    // to incorporate energy loss, multiple scattering
    // and a simple adaptive algorithm to ensure that single steps end approximately
    // at boundaries betwen different experimental features
    
    // The "experimental geometry" is defined in the class Geometry:
    //    * An experiment is a collection of "volumes", that are numbered with a unique
    //      identifier from 0 to the number of experimental features.
    //    * Currently all experiemntal features are cuboids of different sizes and materials,
    //      with the sides aligned to the coordinate system axes. The example is a block of iron
    //      (user-definable length) + two "planar detectors"
    //    * Internally to the "Geometry" class are two helper classes that implement the
    //      formulas for calculation of energy loss (class "Energy loss")
    //      and multiple scattering angles (class "MCS")
    //    * The main functionality is to check, if a certain particle is inside a certain volume
    //      and apply energy loss and multiple scattering during simulation
    //    * The class also provides a simple mechanism to detect changes in the volume as
    //      the particle propagates and suggest an adapted step length to keep one step within
    //      one volume (the granularity of this scan is adjusted with "minfeaturesize")
    //    * At the end, the class is used to "detect" particle signals in certain volumes (detectors)
    //
    //
    // At the end of the simulation of each event, one may analyse the
    // results and fill histograms. Examples are provided to perform calculations using the:
    //    * Generated particles (Particles_gen)
    //    * Simulated particles (Particles_sim) - these include the effect of energy loss and
    //      multiple scattering
    //    * Detector response (Particles_det) - these provide measurement points that can be
    //      further used to reconstruct particles like in a real experiment, where

    // parameters used for the ParticleTracker
    // total time to track (seconds), number of time steps, use/don't use RK4
    static double time = 6E-8;
    static int nsteps = 1000;
    static final boolean useRungeKutta4 = true;

    // minimum size of experimental features,
    // ensure this is a factor ~10 smaller than the thinest elements of the experiment
    static final double minfeaturesize = 0.005;

    // Number of events to simulate
    static final int numberOfEvents = 2022;
    static int n = 0;   //This number keeps track of how many particles didn't reach the final detector
    static int z = 16;  //Length of experiment in z direction, 16 required for 100% detected, true ATLAS length is 36m
    static double [] Raw_energy_final = new double [2022];  //Array for output of raw data
    static double [] finalMassGen = new double [2022];      //Array for output of data from Particles_gen
    static double [] finalMassSim = new double [2022];     //Array for output of data from Particles_sim
    static double [] finalMass = new double [2022];     //Array for output of final data
    static boolean remove = false;                      //Whether to remove this group of muons or not
    
    public static void main (String [] args ) throws Exception
    {
        // setup histogram for analysis
        Histogram hist_Final_Mass_Gen = new Histogram(100, 0, 600, "Measured mass");
        Histogram hist_Final_Mass_Sim = new Histogram(100, 0, 600, "Measured mass");
        Histogram hist_Final_Mass = new Histogram(100, 0, 600, "Measured mass");
        //Calls the method readFile for extraction of the data
        readFile();
        
        // Define the genotrical properties of the experiment in method SetupExperiment()
        Geometry Experiment = SetupExperiment();
        
        // start of main loop: run the simulation numberOfEvents times
        for (int nev = 0; nev < numberOfEvents; nev++) {

            if (nev % 500 == 0) {
                System.out.println("Simulating event " + nev);
            }

            // get the particles of the event to simulate
            Particle [] Particles_gen = GetParticles(nev);

            // simulate propagation of each generated particle,
            // store output in Particles_sim and Tracks_sim
            Particle [] Particles_sim = new Particle[Particles_gen.length];
            Track [] Tracks_sim = new Track[Particles_gen.length];
            double [][] fourVec = new double [4][4];
            double [][] fourVec1 = new double [4][4];
            
            for (int ip = 0; ip < Particles_gen.length; ip++) {
                // some output (need to disable before running large numbers of events!)
                // System.out.println("Simulating particle " + ip + " of event " + nev);
                //Particles_gen[ip].print();
                    
                ParticleTracker tracker = new ParticleTracker(Particles_gen[ip], time, nsteps);
                Particles_sim[ip] = tracker.track(Experiment);

                //This is for assigning Particles Gen/Sim to four vectors for their final mass calculation
                fourVec[ip][0] = Particles_gen[ip].E();
                fourVec[ip][1] = Particles_gen[ip].px;
                fourVec[ip][2] = Particles_gen[ip].py;
                fourVec[ip][3] = Particles_gen[ip].pz;
                if(ip==3)
                {
                    finalMassGen[nev] = GetFinalMass(fourVec);
                }
                
                fourVec1[ip][0] = Particles_sim[ip].E();
                fourVec1[ip][1] = Particles_sim[ip].px;
                fourVec1[ip][2] = Particles_sim[ip].py;
                fourVec1[ip][3] = Particles_sim[ip].pz;
                if(ip==3)
                {
                    finalMassSim[nev] = GetFinalMass(fourVec1);
                }

                //Particles_sim[ip].print();

                // save the full simulated track for later analysis
                Tracks_sim[ip] = tracker.getTrack();
                

                // write scatter plot for first event, particles 0,1,2 and 3 to disk into file "output_particle#.csv" where # is particle no.
//                if (nev == 0 && ip == 0) {
//                    Tracks_sim[ip].writeToDisk("output_particle"+ ip +".csv");
//                }
//                if (nev == 0 && ip == 1) {
//                    Tracks_sim[ip].writeToDisk("output_particle"+ ip +".csv");
//                }
//                if (nev == 0 && ip == 2) {
//                    Tracks_sim[ip].writeToDisk("output_particle"+ ip +".csv");
//                }
//                if (nev == 0 && ip == 3) {
//                    Tracks_sim[ip].writeToDisk("output_particle"+ ip +".csv");
//                }
            }
            // end of simulated particle propagation

            
            // simulate detection of each particle in each element from the simulated tracks
            // this is just for dumping the simulation to the screen (i.e. debug particles by volume)       
             for (int ip = 0; ip < Tracks_sim.length; ip++) {
              double [][] detection_txyz = Experiment.detectParticles(Tracks_sim[ip]);
                 for (int idet = 1; idet < Experiment.getNshapes(); idet++) {
//                     System.out.println("Particle " + ip + " detection in volume " + idet);
//                     System.out.println("(t,x,y,z) = (" + detection_txyz[idet][0] + ", "
//                                     + detection_txyz[idet][1] + ", "
//                                     + detection_txyz[idet][2] + ", "
//                                     + detection_txyz[idet][3] + ")");
//                    System.out.println("Radius detection = "+ Math.sqrt((detection_txyz[idet][1]*detection_txyz[idet][1])+
//                                     (detection_txyz[idet][2]*detection_txyz[idet][2])));
                    

                    //This keeps tracks of how many particles didn't make the final detector (r<2.5 for volume 7 in if statement)
                    if((Math.sqrt((detection_txyz[idet][1]*detection_txyz[idet][1])+(detection_txyz[idet][2]*detection_txyz[idet][2]))<2.5)&&(idet==7))
                    {
                        n++;
                    }
                 }
             }
             
             if (nev==numberOfEvents-1)
             {
                 System.out.println(n + " Particles didn't make the detector");
             }

//            Smearing
//            Random x = new Random();
//            double dDetectorx = x.nextGaussian() * 0.005;
//            Random y = new Random();
//            double dDetectory = y.nextGaussian() * 0.005;
//            Random z = new Random();
//            double dDetectorz = z.nextGaussian() * 0.005;
//            Random j = new Random();
//            double dDetectorj = j.nextGaussian() * 0.005;
            //after detection: reconstruct the angle from the two detected positions!
            // the detectors have volume number 2+3 (see printout)
            
            
            //Reconstruction section
            
            //Initialisation of variables for reconstruction
            double [] x_det1 = new double [4];
            double [] x_det2 = new double [4];
            double [] x_det3 = new double [4];
            double [] y_det1 = new double [4];
            double [] y_det2 = new double [4];
            double [] y_det3 = new double [4];
            double [] z_det1 = new double [4];
            double [] z_det2 = new double [4];
            double [] z_det3 = new double [4];
            double [] xy_det0;
            double [][] Energy_array = new double [2022][4];
            double [][] fourVec2 = new double [4][4];
            
            //Loop for 4 particles
            
            for(int k = 0; k < 4; k++)
            {
            double [][] detection_txyz = Experiment.detectParticles(Tracks_sim[k]);
            x_det1 [k] = detection_txyz[3][1]; // x-coo in detector 1
            x_det2 [k] = detection_txyz[5][1]; // x-coo in detector 2
            x_det3 [k] = detection_txyz[7][1]; // x-coo in detector 3
            y_det1 [k] = detection_txyz[3][2]; // y-coo in detector 1
            y_det2 [k] = detection_txyz[5][2]; // y-coo in detector 2
            y_det3 [k] = detection_txyz[7][2]; // y-coo in detector 3
            z_det1 [k] = detection_txyz[3][3]; // z-coo in detector 1
            z_det2 [k] = detection_txyz[5][3]; // z-coo in detector 2
            z_det3 [k] = detection_txyz[7][3]; // z-coo in detector 3
            double r = Math.sqrt((x_det3[k]*x_det3[k])+(y_det3[k]*y_det3[k]));
            
            //Print statements (for debugging)
//            int particle = k+1;
//            int Event = nev+1;
//            System.out.println("Particle: " + particle + ", Event:" + Event);
//            System.out.println("x"+ k +": " + x_det3[k]+","+"Event #:"+Event);
//            System.out.println("y"+ k +": " + y_det3[k]+","+"Event #:"+Event);
//            System.out.println("z"+ k +": " + z_det3[k]+","+"Event #:"+Event);
//            System.out.println("r"+ k +": " + r+","+"Event #:"+Event);
            
            double m12 = Reconstruction.getm12(y_det2[k], y_det1[k], x_det2[k], x_det1[k]);
            double m13 = Reconstruction.getm13(y_det3[k], y_det1[k], x_det3[k], x_det1[k]);
            double m23 = Reconstruction.getm23(y_det3[k], y_det2[k], x_det3[k], x_det2[k]);
            double m = Reconstruction.average3In(m13, m12, m23);
            double c1 = Reconstruction.getC(y_det1[k], x_det1[k], m);
            double c2 = Reconstruction.getC(y_det2[k], x_det2[k], m);
            double c3 = Reconstruction.getC(y_det3[k], x_det3[k], m);
            double c = Reconstruction.average3In(c1, c2, c3);
            double x_neg = Reconstruction.quadraticXNegative(m, c);
            double x_pos = Reconstruction.quadraticXPositive(m, c);
            double y_neg = m*x_neg+c;
            double y_pos = m*x_pos+c;
            xy_det0 = Reconstruction.getClosest(x_det1[k], y_det1[k], x_pos, x_neg, y_pos, y_neg);
            double x_det0 = xy_det0[0];
            double y_det0 = xy_det0[1];
            
            if (m12 != m13)
            {
                //System.out.println("The particle is travelling on a curved path through the detectors");
            }
            double deltarecon = Reconstruction.getdeltarecon(x_det0, y_det0, m);
            double theta1 = Reconstruction.getTheta(x_det1[k],y_det1[k],z_det1[k]);
            double theta2 = Reconstruction.getTheta(x_det2[k],y_det2[k],z_det2[k]);
            double theta3 = Reconstruction.getTheta(x_det3[k],y_det3[k],z_det3[k]);
            double theta = Reconstruction.average3In(theta1, theta2, theta3);
            double Qrecon = Reconstruction.getQrecon(deltarecon);
            double Phirecon = Reconstruction.getPhirecon(x_det0, y_det0);
            double phirecon = Reconstruction.getphirecon(Phirecon, deltarecon);
            double Pperprecon = Reconstruction.getPperprecon(Qrecon, deltarecon);
            double p = Pperprecon/Math.sin(theta);
            //Variables for construction of the final four vectors
            double px = Pperprecon*Math.cos(phirecon);
            double py = Pperprecon*Math.sin(phirecon);
            double pz = p*Math.cos(theta);
            double Erecon = Reconstruction.getErecon(pz, Pperprecon);
            //Test if p is not a number, if it is it sets it to zero (to aid with below, removing it from the output)
            if (Double.isNaN(p)==true)
            {
                p=0;
                px=0;
                py=0;
                pz=0;
            }
//          System.out.println("Real pz: " + pz0[k] + ",my pz: " + pz);

            Energy_array[nev][k]=Erecon;    //Array of the energies
            
            //Tests if the energy is not a number, if it is it sets all energies from the relevant event to -0.25 totalling -1 later on
            //This is due to the quadratic formulae returning a negative root
            if ((Double.isNaN(Energy_array[nev][k])==true)||(remove==true))
            {
                Energy_array[nev][0]=-0.25;
                Energy_array[nev][1]=-0.25;
                Energy_array[nev][2]=-0.25;
                Energy_array[nev][3]=-0.25;
                remove = true;
                if (k==3){
                    remove = false;
                    finalMass[nev] = -1;
                }
            }
            else
            {
            //Setup four vectors for the calculation of the final mass
                fourVec2[k][0] = Energy_array[nev][k];
                fourVec2[k][1] = px;
                fourVec2[k][2] = py;
                fourVec2[k][2] = pz;
                if(k==3)
                {
                    finalMass[nev] = GetFinalMass(fourVec2);    //Add final mass to array, convert it to GeV
                }
            }
            
            //Print statements (for debugging)
            //System.out.println("deltarecon: " + deltarecon);
//            System.out.println("Qrecon: " + Qrecon);
//            System.out.println("Phirecon: " + Phirecon);
//            System.out.println("phirecon: " + phirecon);
//            System.out.println("Pperprecon: " + Pperprecon/1E3 + " GeV");
//            System.out.println("Erecon: " + Erecon);
//            System.out.println("E" + k + ": " + (Energy_array[nev][k]) + " GeV");
            }
            
            //Here the final energy is calculated for the raw data output
            Raw_energy_final [nev] = (Energy_array[nev][0] + Energy_array[nev][1] + Energy_array[nev][2] + Energy_array[nev][3]);
            
            //Filling of the histograms
            hist_Final_Mass_Gen.fill(finalMassGen[nev]);
            hist_Final_Mass_Sim.fill(finalMassSim[nev]);
            hist_Final_Mass.fill(finalMass[nev]);
        } 
// end of main event loop

        //Write out raw data
        writeEnergyToDisk("RawData_FinalEnergy.csv");
        writeMassToDisk("RawData_FinalMass_ParticlesGenSim.csv");
        //Write out histograms
        hist_Final_Mass_Gen.writeToDisk("Final_Mass_Gen.csv");
        hist_Final_Mass_Sim.writeToDisk("Final_Mass_Sim.csv");
        hist_Final_Mass.writeToDisk("Final_Mass.csv");
    } 
// end of main

    //Setup of geometry of the experiment
    
    public static Geometry SetupExperiment ()
    {
        Geometry Experiment = new Geometry(minfeaturesize);
        
        // this line defines the size of the experiment in vacuum
        Experiment.AddLayer(0,                
                             2.6, 
                             z,
                             0., 0., 0.);                   // zeros for "vacuum"
        
        Experiment.AddLayer(0,                
                             1.2, 
                             z,
                             0., 0., 0.);                   // zeros for "vacuum"

        // block of iron: 0.4x0.4 m^2 wide in x,y-direction, ironThickness m thick in z-direction
        Experiment.AddLayer(1.2,            
                             2.2,
                             z,
                             7.87, 26, 55.845);          // density, Z, A; 
        
        // three 1mm-thin "silicon detectors" 10cm, 20cm and 30cm after the iron block
        Experiment.AddLayer(2.3,
                             2.31,
                             z,
                             2.33, 14, 28.0855);                // density, Z, A 
        
        Experiment.AddLayer(2.31,
                             2.4,
                             z,
                            0, 0, 0 );                 // density, Z, A
        
        Experiment.AddLayer(2.4,
                             2.41,  
                             z,
                             2.33, 14, 28.0855);                // density, Z, A
        
        Experiment.AddLayer(2.41,
                             2.5,
                             z,
                             0, 0, 0);                 // density, Z, A
        
        Experiment.AddLayer(2.5,
                            2.51,
                            z,
                            2.33, 14, 28.0855);               // density, Z, A
        
        Experiment.Print();

        return Experiment;
    } // end Geometry
  
    //Returns mass in GeV for Particles_gen and sim
    public static double GetFinalMass(double [] [] fourVec)
    {
        double [] fourVecFinal = {0,0,0,0};
        
                    for(int i=0;i<4;i++)
                    {
                    fourVecFinal [0] = fourVecFinal [0] + fourVec[i][0];
                    fourVecFinal [1] = fourVecFinal [1] + fourVec[i][1];
                    fourVecFinal [2] = fourVecFinal [2] + fourVec[i][2];
                    fourVecFinal [3] = fourVecFinal [3] + fourVec[i][3];
                    }
                    
                    double p1 = fourVecFinal[1]*fourVecFinal[1] + fourVecFinal[2]*fourVecFinal[2] + fourVecFinal[3]*fourVecFinal[3];
                    double p = Math.sqrt(p1);
                    double m = (Math.sqrt((fourVecFinal[0]*fourVecFinal[0])-(p*p)))/1E3;
        return m;
    }
    
        //Methods for writing the output of raw data for Mass and Energy
    public static void writeMassToDisk(String filename)
    {
        PrintWriter outputFile;
        try {
            outputFile = new PrintWriter(filename);
        } catch (IOException e) {
            System.err.println("Failed to open file " + filename + ". Track data was not saved.");
            return;
        }

        // now make a loop to write the contents of the arrays to disk, one number at a time
        outputFile.println("Event number, Final Gen Mass (GeV), Final Sim Mass (GeV)");
        for (int j = 0; j < 2022; j++) {
            // comma separated values
            double [] m_Final = new double [2022];
            m_Final [j] = finalMassGen[j];
            double [] m_Final1 = new double [2022];
            m_Final1 [j] = finalMassSim[j];
            outputFile.println(j + "," + m_Final[j] + "," + m_Final1[j]);
        }
        outputFile.close(); // close the output file
    }
    
        public static void writeEnergyToDisk(String filename)
    {
        PrintWriter outputFile;
        try {
            outputFile = new PrintWriter(filename);
        } catch (IOException e) {
            System.err.println("Failed to open file " + filename + ". Track data was not saved.");
            return;
        }

        // now make a loop to write the contents of the arrays to disk, one number at a time
        outputFile.println("Event number, Final Energy (GeV)");
        for (int j = 0; j < 2022; j++) {
            // comma separated values
            double [] E_Final = new double [2022];
            E_Final [j] = Raw_energy_final[j];
            outputFile.println(j + "," + E_Final[j]);
        }
        outputFile.close(); // close the output file
    }
    
    //Method for data input and extraction
    
            //Muon 1
        static double [] px1 = new double [2022]; 
        static double [] py1 = new double [2022]; 
        static double [] pz1 = new double [2022]; 
        static int [] q1 = new int [2022]; 
        //Muon 2
        static double [] px2 = new double [2022]; 
        static double [] py2 = new double [2022]; 
        static double [] pz2 = new double [2022]; 
        static int [] q2 = new int [2022]; 
        //Muon 3
        static double [] px3 = new double [2022]; 
        static double [] py3 = new double [2022]; 
        static double [] pz3 = new double [2022]; 
        static int [] q3 = new int [2022]; 
        //Muon 4
        static double [] px4 = new double [2022]; 
        static double [] py4 = new double [2022]; 
        static double [] pz4 = new double [2022]; 
        static int [] q4 = new int [2022];
    
        public static void readFile() throws Exception
    {
        File file = new File ("C:\\Users\\James\\Documents\\Uni\\#PHYS 305 - Computational modelling\\HighMassH4l.txt");
        Scanner s = new Scanner(file);
        int nstep = 0;
        
        while(nstep<=2021)
        {
        //Stored from 0 - 2021
        px1[nstep] = (s.nextInt());
        py1[nstep] = (s.nextInt());
        pz1[nstep] = (s.nextInt());
        q1[nstep] = (s.nextInt());
        
        px2[nstep] = (s.nextInt());
        py2[nstep] = (s.nextInt());
        pz2[nstep] = (s.nextInt());
        q2[nstep] = (s.nextInt());
        
        px3[nstep] = (s.nextInt());
        py3[nstep] = (s.nextInt());
        pz3[nstep] = (s.nextInt());
        q3[nstep] = (s.nextInt());
        
        px4[nstep] = (s.nextInt());
        py4[nstep] = (s.nextInt());
        pz4[nstep] = (s.nextInt());
        q4[nstep] = (s.nextInt());
        nstep ++;
        }
    }
        
    public static Particle[] GetParticles(int j)
    {
        // example to simulate just one muon starting at (0,0,0)
        // we follow the particle physics "convention"
        // to have the z-axis in the (approximate) direction of the beam

        // this just sets up the array (for a case where one event has more than one particle)
        Particle [] Particles_gen = new Particle[4];

        //for(int j = 0; j < 2022; j++)
        {
        for(int i = 0; i < Npart; i++)
        {
            // create particle and set properties
            Particles_gen[i] = new Particle();
            Particles_gen[i].m = 106;
            // initial position for all particles (x,y,z) = (0,0,0)
            Particles_gen[i].x = 0.;
            Particles_gen[i].y = 0.;
            Particles_gen[i].z = 0.;
            
            switch(i){
                case 0: 
                    {
                    Particles_gen[i].px = px1[j];
                    Particles_gen[i].py = py1[j];
                    Particles_gen[i].pz = pz1[j];
                    Particles_gen[i].Q = q1[j];
                    break;
                    }
                case 1: 
                {
                    Particles_gen[i].px = px2[j];
                    Particles_gen[i].py = py2[j];
                    Particles_gen[i].pz = pz2[j];
                    Particles_gen[i].Q = q2[j];
                    break;
                }
                case 2: 
                {
                    Particles_gen[i].px = px3[j];
                    Particles_gen[i].py = py3[j];
                    Particles_gen[i].pz = pz3[j];
                    Particles_gen[i].Q = q3[j];
                    break;
                }
                case 3: 
                {
                    Particles_gen[i].px = px4[j];
                    Particles_gen[i].py = py4[j];
                    Particles_gen[i].pz = pz4[j];
                    Particles_gen[i].Q = q4[j];
                    break;
                }      
        }
    }
   }
    return Particles_gen;
    }
}

